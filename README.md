# OpenML dataset: edm

https://www.openml.org/d/41395

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Multivariate regression data set from: https://link.springer.com/article/10.1007%2Fs10994-016-5546-z : The Electrical Discharge Machining dataset (Karalic and Bratko 1997) represents a two-target regression problem. The task is to shorten the machining time by reproducing the behaviour of a human operator that controls the values of two variables. Each of the target variables takes 3 distinct numeric values (  -1,0,1 ) and there are 16 continuous input variables.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41395) of an [OpenML dataset](https://www.openml.org/d/41395). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41395/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41395/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41395/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

